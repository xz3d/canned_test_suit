import json
from datetime import datetime, timedelta

import os
import sys

def roll_over_day(date: str):
    res = (datetime.strptime(date, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
    return res

def execute(filename):
    with open(filename, 'r') as a:
        with open('temp.json', 'w') as t:
            r = a.read()
            js = json.loads(r)
            js_copy = js
            print(str(js))
            res1 = roll_over_day(js[0]['START_DATE'])
            res2 = roll_over_day(js[0]['END_DATE'])
            js_copy[0]['START_DATE'] = res1
            js_copy[0]['END_DATE'] = res2
            x = json.dumps(js_copy)
            print(str(js_copy))
            t.write(x)
            # rename the file and delete original file
            os.remove(filename)
            os.rename('temp.json', filename)

if __name__ == "__main__":
    execute(sys.argv[1])