now1=$(date)
echo $now1

echo "############## S t a r t ############################"
python3 format_output_header.py
python3 test_case_eval.py assert --api api_container_cost_fetch_ecs_ct --sno 1
python3 test_case_eval.py assert --api api_container_cost_fetch_ecs_sv --sno 2
python3 test_case_eval.py assert --api api_container_cost_fetch_ecs_tk --sno 3
python3 test_case_eval.py assert --api api_container_cost_fetch_eks_ct --sno 4
python3 test_case_eval.py assert --api api_container_cost_summary_fetch --sno 5
python3 test_case_eval.py assert --api api_explorer_container_tabdata_fetch --sno 6
python3 test_case_eval.py assert --api api_explorer_dimlist --sno 7
python3 test_case_eval.py assert --api api_explorer_overview_summary --sno 8
python3 test_case_eval.py assert --api api_explorer_pie_chart --sno 9
sleep 1m
python3 test_case_eval.py assert --api api_explorer_sankeydata --sno 10
python3 test_case_eval.py assert --api api_extended_summary --sno 11
python3 test_case_eval.py assert --api api_extended_summarypie --sno 12
python3 test_case_eval.py assert --api api_heirarchy --sno 13
python3 test_case_eval.py assert --api api_opt_costinstances_fetch_ii --sno 14
python3 test_case_eval.py assert --api api_opt_costinstances_fetch_itc --sno 16
python3 test_case_eval.py assert --api api_opt_costinstances_fetch_ss --sno 17
python3 test_case_eval.py assert --api api_opt_costinstances_fetch --sno 18
python3 test_case_eval.py assert --api api_scalinggroup_cost_fetch --sno 19
python3 test_case_eval.py assert --api api_scalinggroup_cost_summary_fetch --sno 20
python3 test_case_eval.py assert --api api_servedomain_agg_summary_fetch --sno 21
python3 test_case_eval.py assert --api api_servedomain_summary_fetch --sno 23
python3 test_case_eval.py assert --api api_showcostover_list --sno 26
python3 test_case_eval.py assert --api api_stackbar_fin --sno 27
sleep 1m
python3 test_case_eval.py assert --api api_storages_cost_fetch --sno 28
python3 test_case_eval.py assert --api api_summary_fetch --sno 29
python3 test_case_eval.py assert --api api_gov_name_res --sno 195
python3 test_case_eval.py assert --api api_reservation_summary_list --sno 196
python3 test_case_eval.py assert --api api_reservations_details_fetch --sno 197
sleep 1m
python3 test_case_eval.py assert --api api_reservation_allocation_timeseries --sno 216
sleep 1m
python3 test_case_eval.py assert --api api_reservation_cost_timeseries --sno 235
python3 test_case_eval.py assert --api api_reservation_usage_timeseries --sno 254
python3 test_case_eval.py assert --api api_environmentlist --sno 273
python3 test_case_eval.py assert --api api_userlist --sno 274
python3 test_case_eval.py assert --api api_userproplist --sno 275
python3 test_case_eval.py assert --api api_accounts --sno 276
echo "############## F i n i s h ############################"
echo ------------------------------------------------------------------------------------------------
now=$(date)
echo $now
echo ------------------------------------------------------------------------------------------------