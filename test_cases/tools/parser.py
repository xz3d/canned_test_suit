import argparse
from tools.bearer_token import *
from tools.framework import *

def arghandle(args, tcname):
    if args.command == 'collect':
        aq_collect(args.api)
    elif args.command == 'assert':
        aq_assert(args.api, tcname, args.sno)

def test_case_init(tcname):
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(dest='command')
    coll = subparser.add_parser('collect')
    ae = subparser.add_parser('assert')
    coll.add_argument('--api', type=str, default='api_summary_fetch', help='Collect Golden Data')
    ae.add_argument('--api', type=str, default='api_summary_fetch', help='Assert on Golden Data')
    ae.add_argument('--sno', type=int, default=1)
    args = parser.parse_args()
    arghandle(args, tcname)