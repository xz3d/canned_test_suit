import sys
sys.path.append("/mnt/c/t/aquila/celebrimbor/scenario/test_home")

from tools.bearer_token import *
from tools.framework import *
from tools.parser import *

global num

test_case_init("api_container_cost_fetch_ecs_ct")
test_case_init("api_container_cost_fetch_ecs_sv")
test_case_init("api_container_cost_fetch_ecs_tk")
test_case_init("api_container_cost_fetch_eks_ct")
test_case_init("api_container_cost_summary_fetch")
test_case_init("api_explorer_container_tabdata_fetch")
test_case_init("api_explorer_dimlist")
test_case_init("api_explorer_overview_summary")
test_case_init("api_explorer_pie_chart")
test_case_init("api_explorer_sankeydata")
test_case_init("api_extended_summary")
test_case_init("api_extended_summarypie")
test_case_init("api_heirarchy")
test_case_init("api_opt_costinstances_fetch_ii")
test_case_init("api_opt_costinstances_fetch_itc")
test_case_init("api_opt_costinstances_fetch_ss")
test_case_init("api_opt_costinstances_fetch")
test_case_init("api_scalinggroup_cost_fetch")
test_case_init("api_scalinggroup_cost_summary_fetch")
test_case_init("api_servedomain_agg_summary_fetch")
test_case_init("api_servedomain_summary_fetch")
test_case_init("api_showcostover_list")
test_case_init("api_stackbar_fin")
test_case_init("api_storages_cost_fetch")
test_case_init("api_summary_fetc")
test_case_init("api_gov_name_res")
test_case_init("api_reservation_summary_list")
test_case_init("api_reservations_details_fetch")
test_case_init("api_reservation_alloc\ation_timeseries")
test_case_init("api_reservation_cost_timeseries")
test_case_init("api_reservation_usage_timeseries")
test_case_init("api_environmentlist")
test_case_init("api_userlist")
test_case_init("api_userproplist")
test_case_init("api_accounts")