python3 test_case_eval.py collect --api api_container_cost_fetch_ecs_ct
python3 test_case_eval.py collect --api api_container_cost_fetch_ecs_sv
python3 test_case_eval.py collect --api api_container_cost_fetch_ecs_tk
python3 test_case_eval.py collect --api api_container_cost_fetch_eks_ct
python3 test_case_eval.py collect --api api_container_cost_summary_fetch
python3 test_case_eval.py collect --api api_explorer_container_tabdata_fetch
python3 test_case_eval.py collect --api api_explorer_dimlist
python3 test_case_eval.py collect --api api_explorer_overview_summary
python3 test_case_eval.py collect --api api_explorer_pie_chart
sleep 1m
python3 test_case_eval.py collect --api api_explorer_sankeydata
python3 test_case_eval.py collect --api api_extended_summary
python3 test_case_eval.py collect --api api_extended_summarypie
python3 test_case_eval.py collect --api api_heirarchy
python3 test_case_eval.py collect --api api_opt_costinstances_fetch_ii
python3 test_case_eval.py collect --api api_opt_costinstances_fetch_itc
python3 test_case_eval.py collect --api api_opt_costinstances_fetch_ss
python3 test_case_eval.py collect --api api_opt_costinstances_fetch
python3 test_case_eval.py collect --api api_scalinggroup_cost_fetch
python3 test_case_eval.py collect --api api_scalinggroup_cost_summary_fetch
python3 test_case_eval.py collect --api api_servedomain_agg_summary_fetch
python3 test_case_eval.py collect --api api_servedomain_summary_fetch
python3 test_case_eval.py collect --api api_showcostover_list
python3 test_case_eval.py collect --api api_stackbar_fin
sleep 1m
python3 test_case_eval.py collect --api api_storages_cost_fetch
python3 test_case_eval.py collect --api api_gov_name_res
python3 test_case_eval.py collect --api api_reservation_summary_list
python3 test_case_eval.py collect --api api_reservations_details_fetch
sleep 1m
python3 test_case_eval.py collect --api api_reservation_allocation_timeseries
sleep 1m
python3 test_case_eval.py collect --api api_reservation_cost_timeseries
python3 test_case_eval.py collect --api api_reservation_usage_timeseries
python3 test_case_eval.py collect --api api_environmentlist
python3 test_case_eval.py collect --api api_userlist
python3 test_case_eval.py collect --api api_userproplist
python3 test_case_eval.py collect --api api_accounts