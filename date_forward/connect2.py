import psycopg2
import paramiko
from sshtunnel import SSHTunnelForwarder

mypkey = paramiko.RSAKey.from_private_key_file('ora-eng-ssh-key-2020-12-31.key')

tunnel =  SSHTunnelForwarder(
        ('129.159.45.227', 22),
        ssh_username='ubuntu',
        ssh_pkey=mypkey,
        remote_bind_address=('10.0.0.14', 5432))

tunnel.start()
print("Tunn")
try:
    conn = psycopg2.connect(dbname='postgres', user='postgres', password='postgres', host='10.0.0.14', port=5432)
except:
    print("Cannot connect")
