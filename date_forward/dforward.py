from datetime import time
from connect import *

p_host = '10.0.0.14'
p_port = 5432
db = 'postgres'
ssh = True
ssh_user = 'ubuntu'
ssh_host = '129.159.45.227'
ssh_pkey = 'ora-eng-ssh-key-2020-12-31.key'


pgres = aquila_connect_db(pgres_host=p_host, pgres_port=p_port, db=db, ssh=ssh, ssh_user=ssh_user, ssh_host=ssh_host, ssh_pkey=ssh_pkey)

pgres.schemas(db='postgres')

date_forwarding_queries[] = [
    "CREATE TABLE IF NOT EXISTS a2i._awscost_line_item_qa_ AS (SELECT * FROM a2i.awscost_line_item)",
    "CREATE TABLE IF NOT EXISTS a2i._azure_cost_usage_qa_ AS (SELECT * FROM a2i.azure_cost_usage)",
    "CREATE TABLE IF NOT EXISTS a2i._gcp_billing_bigquery_data_qa_ AS (SELECT * FROM a2i.gcp_billing_bigquery_data)",
    "CREATE TABLE IF NOT EXISTS a2i._cloud_instance_qa_ AS (SELECT * FROM a2i.cloud_instance)",
    "UPDATE a2i._awscost_line_item_qa_ SET usage_end_date = usage_end_date + interval '1 day', usage_start_date = usage_start_date + interval '1 day'",
    "UPDATE a2i._gcp_billing_bigquery_data_qa_ SET usage_end_time = usage_end_time + interval '1 day', usage_start_time = usage_start_time + interval '1 day'",
    "UPDATE a2i._azure_cost_usage_qa_ SET usage_end_date = usage_end_date + interval '1 day', usage_start_date = usage_start_date + interval '1 day'",    
    "UPDATE a2i._cloud_instance_qa_ SET timestamp = timestamp + interval '1 day'",
]

for q in date_forwarding_queries:
    query_df = pgres.query(db='database_name', query=q)